---
title: Diplôme inter-universitaire - Enseigner l'informatique au lycée
subtitle: Université de Lille 
author: |
	| Benoit Papegay — Philippe Marquet
	| 
	| diu-eil@univ-lille.fr
date: mai 2019
lang: fr
---

<!-- à compiler par
pandoc -t beamer --slide-level 2 -V "aspectratio=1610" -s 2019-06-diueil.md -o 2019-06-diueil.pdf
--> 

Formation des futurs enseignants de la spécialité _Numérique et
sciences informatiques_ 

* Réforme du baccalauréat — enseignement d'informatique
* Diplôme inter-universitaire — Enseigner l'informatique au lycée
* Enseignement de l'informatique à l'université de Lille
* DIU Enseigner l'informatique au lycée — Université de Lille

La réforme du baccalauréat — enseignement d'informatique
========================================================

Une volonté politique
---------------------

### Le numérique au service de l'École de la confiance

:::::: {.columns}
::: {.column width="25%"}

![](figs/DP-LUDOVIA_987361-1.pdf)

:::
::: {.column width="70%"}
#### Axes principaux

* placer les données scolaires au cœur de la stratégie numérique du ministère
* enseigner au XXIe siècle avec le numérique 
* accompagner et renforcer le développement professionnel des professeurs 
* développer les **compétences numériques des élèves**
* créer de nouveaux liens avec les acteurs et les partenaires de l’école

#### Actions engagées

* réforme du collège
* réforme du lycée : SNT et NSI
* CAPES d'informatique 
:::
::::::

Sciences numériques et technologie -- SNT
-----------------------------------------

:::::: {.columns}
::: {.column width="25%"}

![](figs/spe633_annexe_1063268-1.pdf)

:::
::: {.column width="70%"}

Nouvel enseignement **obligatoire de seconde**
        
#### Fournir une culture générale numérique aux élèves

* **concepts** : données et informations, algorithmes, langages et
  programmes, machines \
  \+ interfaces
* **notions  transversales** de programmation (Python)

#### Thématiques

~4 semaines $\times$ 1h30 par thème

:::::: {.columns}
::: {.column width="25%"}

* internet
* le _Web_
* les réseaux sociaux
:::
::: {.column width="75%"}
* les données structurées et leur traitement
* localisation, cartographie et mobilité
* informatique embarquée et objets connectés
* la photographie numérique
:::
::::::

:::
::::::

Numérique et sciences informatiques en 1re -- NSI
-------------------------------------------------

:::::: {.columns}
::: {.column width="25%"}

![](figs/spe641_annexe_1063085-1.pdf)

:::
::: {.column width="70%"}

**Discipline de spécialité** -- 4h semaine 1re, 6h en term.

Préparation aux études supérieures
        
#### Fondements de l'informatique

* **mêmes concepts**  : données et informations, algorithmes, langages
  et programmes, machines 
* **approfondissement** en programmation
* démarche de **projet**

#### 8 rubriques

* histoire de l’informatique
* représentation des données : types et valeurs de base
* représentation des données : types construits
* traitement de données en tables
* interactions entre l’homme et la machine sur le _Web_
* architectures matérielles et systèmes d’exploitation
* langages et programmation
* algorithmique
:::
::::::

Numérique et sciences informatiques en Terminale -- NSI
-------------------------------------------------------

\centerline{\Large{À paraître...}}

Diplôme inter-universitaire — Enseigner l'informatique au lycée
===============================================================

Historique de la création du DIU EIL
------------------------------------

* février 2018
  
  * rapport Mathiot sur la réforme du Baccalauréat
  * conférence Didapro 07, Lausanne. Débat : quelle formation pour
    les enseignants NSI ? 
    
* mars - juin 2018 
  
  * mobilisation de réseaux : ATIEF, C3i IREM, Réseau ÉSPE, SIF
    – Société informatique de France
  * journée enseignement SIF 
  * mémorandum de la SIF sur la formation des enseignants
  
* octobre - novembre 2018 
  
  * validation par le CSP du programme NSI de première
  * mise en place du groupe de travail à la DGRH avec MENJ, CPU, SIF
     et porteurs DIU 
  
* janvier 2019  
  
  * annonce du CAPES informatique
  * lancement officiel du DIU EIL

Universités partenaires du DIU EIL
----------------------------------

--------------------  -------------------- --------------------
Aix-Marseille         Picardie - Amiens    Franche-Comté
Bordeaux              Caen                 Clermont Auvergne
Corse                 Paris Est Créteil    Marne la Vallée
Paris Nord            Bourgogne            Grenoble-Alpes
Savoie Mont-Blanc     Antilles             Guyane
La Rochelle           Le Havre             Lille
Limoges               Lyon 1               Montpellier
Lorraine              Nantes               Nice Côte d'Azur
Orléans               CNAM                 Sorbonne Jussieu
Poitiers              Reims                Rennes 1
La Réunion            Rouen                Strasbourg
Haute Alsace          Toulouse III         Paris-Sud
Versailles            Nouvelle-Calédonie   Polynésie Française
--------------------  -------------------- --------------------

Le DIU Enseigner l'informatique au lycée
----------------------------------------

### un public ciblé

* professeurs de lycée, formés à / enseignant ISN 
* n'ayant pas eu de formation initiale suffisante 

### un programme pédagogique national

* cinq blocs de formation de 25h en présentiel + 10 h à distance
* aligné sur programme NSI de première et terminale
  * avec le recul nécessaire à l'enseignant
  * incluant des références didactiques et historiques

### une organisation nationale

* formation diplômante : diplôme interuniversitaire
* groupes de 24 stagiaires
* formation sur 2 ans : année 1 : blocs 1 à 3 – année 2 : blocs 4 et 5
* modalités de validation communes – mobilité entre
  académie / université possible

### une organisation locale négociée entre académies et universités

* calendrier
* recrutement et inscriptions

Les domaines de NSI et les blocs du DIU
---------------------------------------

### classe de première

  * représentation des données : types et valeurs de base – bloc 1
  * représentation des données : types construits – bloc 1
  * traitement de données en tables – bloc 1
  * interactions entre l'homme et la machine sur le Web – bloc 1 et bloc 3
  * architectures matérielles et systèmes d’exploitation – bloc 3
  * langages et programmation – bloc 1
  * algorithmique – bloc 2

### classe de terminale

  * structures de données – bloc 4
  * bases de données – bloc 4
  * architectures matérielles, systèmes d’exploitation et réseaux – bloc 3
  * langages et programmation – bloc 4
  * algorithmique – bloc 5

Objectifs de la formation
-------------------------

### formation disciplinaire et didactique

* accompagnement des enseignants vers NSI
* montée en compétences au niveau informatique
* réflexion pédagogique et didactique
* apprentissage d'un fonctionnement en mode projet

### création de contenus pour NSI

Modalités de formation
----------------------

### principes

* travail en équipe – collaboration, pédagogie par projets,
  co-construction de ressources
* équilibre entre contenu théorique et pratique 
* réflexion individuelle et collective sur la didactique de l'informatique

### formats

  * séances de travaux pratiques sur machine
  * apport théoriques : supports consultables à distance, rappels en séance
  * séances de travail en groupe
  * séances de restitution

### espace numérique de formation

* un espace national sur m@gistère
* des espaces locaux par université

Modalités d'évaluation
----------------------

### double dimension ###

* disciplinaire – validation des compétences en informatique, et
* didactique – validation des compétences en lien avec son
  enseignement

### validation par blocs ###

* sans compensation
* DIU validé si et seulement si l'ensemble des blocs est validé

évaluations sur des réalisations individuelles et/ou collectives

### jury

* jury annuel pour valider les blocs
* validation du diplôme quand les 5 blocs sont validés


Rôles respectifs des universités et des académies
-------------------------------------------------

### organisation et financement

* ministère : cadrage du dispositif
* académie : recensement des besoins, attribue subvention de service
  public par convention 
* université : prestataire de formation

### capacité à enseigner NSI et DIU

* ministère : reconnait aux titulaires du DIU la capacité à enseigner
  NSI, et \
  enregistre l'information dans son système d'information
* académie : les IPR peuvent reconnaître la capacité à enseigner NSI
  sur dossier selon diplômes d'informatique 
* université : délivrance des diplômes

Perspectives pour les titulaires du DIU Enseigner l'informatique au lycée
-------------------------------------------------------------------------

### une demande croissante en enseignants d'informatique

* besoin estimé entre 2000 et 3000 équivalents temps plein
* environ 1500 collègues en formation DIU EIL
* environ 50 postes par an annoncés au CAPES d'informatique
* les volontaires pourront enseigner l'informatique aussi longtemps
  qu'ils le souhaitent 

### des choix et des opportunités

* enseigner NSI en plus de sa discipline d'origine
* demander un changement de discipline vers l'informatique
* aggrégation d'informatique vers 2022/2023

Enjeux et avenir...
-------------------

### des enjeux importants 

* premier plan national de formation continue confié par le MENJ aux universités
* **emergence d'une communauté concernée par l'enseignement de l'informatique**
* attente sociale des élèves et de leurs familles

### l'avenir du DIU

* les universités sont en capacité de répondre à une demande de
  renouvellement de la formation 
* préfiguration du master MEÉF préparant au CAPES informatique


Enseignement de l'informatique à l'université de Lille
======================================================

Formations en informatique à l'université de Lille
--------------------------------------------------

Depuis 50 ans...

### IUT 
* DUT informatique 
* licences professionnelles 


### Faculté des sciences et technologies 
* licence informatique
* masters informatique, MIAGE, SIAD, data science

### Polytech Lille
* départements IMA et GIS

### hors ULille, sur campus cité scientifique
* IMT Lille Douai, école centrale de Lille

Département informatique de l'IUT
---------------------------------

:::::: {.columns}
::: {.column width="55%"}

* 1 DUT informatique – 100 diplômés
* 2 licences professionnelles – 40 diplômés
* 350 étudiants en DUT et licence pro
* 20 alternants / an 
* 150 stages / an
* 47 personnels \
  dont 36 enseignants-chercheurs et enseignants, \
  4 BIATSS \
  \+ 40 professionnels

:::
::: {.column width="45%"}

![](figs/info-iut.png)

:::
::::::

Département informatique de la FST
----------------------------------

Former des cadres pour les métiers de l'informatique

Délivrer un contenu pédagogique fondamental et professionnalisant

\    

:::::: {.columns}
::: {.column width="65%"}

* 2 masters – 150 diplômés
* 1 licence – 200 diplômés
* 750 étudiants du L2 au M2
* 110 alternants 
* 250 stages / an
* 78 personnes \
  dont 56 enseignants-chercheurs et enseignants, \
  10 BIATSS \
  \+ 78 vacataires, dont 53 issus du monde de l'entreprise

:::
::: {.column width="35%"}

![](figs/info-fst.png)

:::
::::::

Formation continue, bientôt initiale des enseignants
----------------------------------------------------

### enseignants d’ISN

* 2011 à 2014 création de la spécialité ISN
* 3 promotions – 60 enseignants au total
* une dizaine de jours sur 2 ans

### enseignants de maths

* 2017/18 – à poursuivre
* aménagement programme de seconde rentrée 2017
* opération académique – 5 sites universitaires
* 200 profs des 90 lycées (800 autres à venir)
* 2 jours (2 autres à venir)

### enseignants de SNT

* 2018/19 à 2019/20
* 500 enseignants sur l’académie
* opération académique – plusieurs sites
* une journée avril 2019, 3 jours 2019/20

Concours de recrutement d'enseignants d'informatique
----------------------------------------------------

### (option informatique au CAPES de maths)

* ouverture à Lille en sept. 2016

### CAPES d’informatique

* annonce Jean-Michel Blanquer, 7 janvier 2019
* ouverture des préparations CAPES septembre 2019
* concours 2020

### préparation CAPES informatique à Lille

* parcours informatique, master MEÉF
* ÉSPÉ Lille Nord de France & université de Lille 
* inscriptions ouvertes

DIU Enseigner l'informatique au lycée — Université de Lille
===========================================================

DIU EIL de l'université de Lille
--------------------------------

### départementS informatiques de l'université de Lille

* DIU diplôme géré par le département informatique de la FST — FIL
* collègues de chacun des départements informatiques
  * FST
  * IUT
  * Polytech Lille

Une équipe pédagogique par bloc de formation

Administrateurs système, secrétariat pédagogique

### "étudiants" / "stagiaires"

72 professeurs du secondaire, répartis en 3 groupes

- répartition géographique – "proximité"

Calendrier
----------

2018/19 – blocs 1 à 3

* 4 jours filés – 9, 10, 15 mai, 5 juin
* 2 semaines temps plein
  - lundi 17 au vendredi 21 juin 
  - lundi 1er au jeudi 4 juillet
  
2019/20 – blocs 4 et 5

* 10 jours filés ... dans l'idéal
  - à définir 

Premières journées
------------------

### jeudi 9 mai 

* 9h00 : amphi de rentrée 
* 10h30-12h15 : _bloc 3_ , TP de prise en main de l'environnement de travail
  - groupe 1 : M5-A12 / groupe 2 : M5-A13 / groupe 3 : M5-A14
* 13h45-17h00 : _bloc 1_ , Travaux pratiques 
  - groupe 1 : M5-A11 / groupe 2 : M5-A13 / groupe 3 : M5-A14

### vendredi 10 mai 

* 9h00-10h30 : _bloc 2_ , Travaux dirigés
  - groupe 1 : M5-A2 / groupe 2 : M5-A3 / groupe 3 : M5-A9
* 10h45-12h15 : _bloc 2_, Travaux pratiques 
  - groupe 1 : M5-A16 / groupe 2 : M5-A13 / groupe 3 : M5-A14
* 13h45-14h45 : _bloc 3_, conférence 
* 15h00-17h00 : _bloc 3_, Travaux pratiques
  - groupe 1 : M5-A11 / groupe 2 : M5-A13 / groupe 3 : M5-A14
  
Portails et ressources
----------------------

### portail de la formation sur le portail du FIL

  - [portail.fil.univ-lille1.fr/fe/diu-eil](http://portail.fil.univ-lille1.fr/fe/diu-eil)

### portail GitLab 

  - depuis le portail du FIL 
  - accès direct [gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md)
  - **info pratiques - ressources**

### forum moodle

  - [moodle.univ-lille1.fr](http://moodle.univ-lille1.fr)
  - depuis le portail GitLab
  - **forum pour chacun des 3 blocs de formation**
  - inscription par clé
	 - groupe 1 : DIU-EIL-G1 / groupe 2 : DIU-EIL-G2 / groupe 3 : DIU-EIL-G3

Infos pratiques
---------------

### salles
  - TP : premier étage du bâtiment M5
  - TD : rez-de-chaussée

accès libre

  - A10 : pour tous les étudiants du FIL
  - A11 : ouverture juste pour vous en dehors des heures de cours

### ressources et services du FIL, de l'université 

* université
  - wifi, adresse mail, moodle, vpn...
* FIL
  - compte associé à la formation
* compte GitLab indépendant

Restauration
------------

  - RU Barrois : paiement par CB
  - salles de convivialité, café
    - maison des étudiants 
	- espace culture
	- Lilliad
	- cafet' du M1
  - restauration rapide – snack, sandwiches, salades...
    - au bâtiment SUP – kiosque
    - MDE
	- 1er étage Lilliad


Bonne "rentrée" !
=================

