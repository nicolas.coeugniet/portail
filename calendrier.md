Calendrier 
==========

[DIU Enseigner l'informatique au lycée](./Readme.md)


jeudi 9 mai 2019
================

C'est la rentrée ! 

| quand       | quoi                                                                                                                   | qui      | où            | avec qui                           |
|-------------|------------------------------------------------------------------------------------------------------------------------|----------|---------------|------------------------------------|
| 8h30        | café d'accueil                                                                                                         |          | hall du M5    |                                    |
| 9h          | amphi de rentrée - [support de présentation](doc/2019-06-diueil-slide.pdf) / [4 par page ](doc/2019-06-diueil-4up.pdf) |          | amphi Bacchus | Benoit Papegay et Philippe Marquet |
| 10h30-12h15 | [bloc 3](bloc3/Readme.md), TP Prise en main de l'environnement de travail                                              | groupe 1 | M5-A12        | Jean-Luc, Yvan                     |
|             |                                                                                                                        | groupe 2 | M5-A13        | Laurent, Philippe                  |
|             |                                                                                                                        | groupe 3 | M5-A14        | Benoit, Jean-François              |
| 13h45-17h   | [bloc 1](bloc1/Readme.md), Travaux pratiques                                                                           | groupe 1 | M5-A11        | Mikaël, Philippe                   |
|             |                                                                                                                        | groupe 2 | M5-A13        | Jean-Christophe, Patricia          |
|             |                                                                                                                        | groupe 3 | M5-A14        | Maude, Éric                        |

Accès au bâtiment M5 : [carte](https://osm.org/go/0B1fzL6bh--?m=) et [plan du campus](https://www.univ-lille.fr/fileadmin/user_upload/autres/Plan-site-Ulille-contact-cite%CC%81-scientifique.pdf)

* l'amphi Bacchus se situe au rdc du bâtiment M5
* les salles A11 à A16 se situent au 1er étage du M5. 


vendredi 10 mai 2019
====================

| quand       | quoi                                  | qui      | où      | avec qui                 |
|-------------|---------------------------------------|----------|---------|--------------------------|
| 9h-10h30    | [bloc 2](bloc2/Readme.md), TD         | groupe 1 | M5-A2   | Marie-Émilie, Benoit     |
|             |                                       | groupe 2 | M5-A3   | Patricia, Bruno Bogaert          |
|             |                                       | groupe 3 | M5-A9   | Philippe, Lucien         |
| 10h45-12h15 | [bloc 2](bloc2/Readme.md), TP         | groupe 1 | M5-A16  | Marie-Émilie, Benoit     |
|             |                                       | groupe 2 | M5-A13  | Patricia, Bruno Bogaert          |
|             |                                       | groupe 3 | M5-A14  | Philippe, Lucien         |
| 13h45-14h45 | [bloc 3](bloc3/Readme.md), conférence |          | Bacchus | Gilles, Samuel, Philippe |
| 15h-17h     | [bloc 3](bloc3/Readme.md), TP         | groupe 1 | M5-A11  | Jean-Luc, Yvan           |
|             |                                       | groupe 2 | M5-A13  | Laurent, Philippe        |
|             |                                       | groupe 3 | M5-A14  | Benoit, Nicolas          |
	
mercredi 15 mai 2019
====================

matin - [bloc 1](bloc1/Readme.md)
--------------

| quand       | quoi                          | qui              | où            | avec qui               |
|-------------|-------------------------------|------------------|---------------|------------------------|
| 9h00-10h00  | Cours                         | tous les groupes | amphi Bacchus | Éric                   |
| 10h15-12h15 | [bloc 1](bloc1/Readme.md), TP | groupe 1         | A11           | Benoit, Jean-Stéphane  |
|             |                               | groupe 2         | A12           | Jean-Christophe,Fabien |
|             |                               | groupe 3         | A13           | Éric, Maude            |


après-midi - [bloc 2](bloc2/Readme.md)
-------------------

| quand       | quoi                          | qui      | où  | avec qui                |
|-------------|-------------------------------|----------|-----|-------------------------|
| 13h45-15h15 | [bloc 2](bloc2/Readme.md), TD | groupe 1 | A6  | Marie-Émilie, Benoit    |
|             |                               | groupe 2 | A7  | Patricia, Bruno Bogaert |
|             |                               | groupe 3 | A8  | Éric, Jean-Stéphane     |
| 15h30-17h00 | [bloc 2](bloc2/Readme.md), TP | groupe 1 | A11 | Marie-Émilie, Benoit    |
|             |                               | groupe 2 | A12 | Patricia, Bruno Bogaert |
|             |                               | groupe 3 | A13 | Éric, Jean-Stéphane     |


mercredi 5 juin 2019
====================

matin - [bloc 1](bloc1/Readme.md)
--------------

| quand       | quoi                          | qui              | où            | avec qui                  |
|-------------|-------------------------------|------------------|---------------|---------------------------|
| 9h00-10h00  | Cours                         | tous les groupes | amphi Bacchus | Éric                      |
| 10h15-12h15 | [bloc 1](bloc1/Readme.md), TD | groupe 1         | A1            | Benoit, Mikaël            |
|             |                               | groupe 2         | A2            | Jean-Christophe, Patricia |
|             |                               | groupe 3         | A3            | Éric, Fabien              |


après-midi - [bloc 3](bloc3/Readme.md)
-------------------

| quand       | quoi                          | qui      | où            | avec qui              |
|-------------|-------------------------------|----------|---------------|-----------------------|
| 13h45-14h45 | Cours                         |          | amphi Bacchus | Philippe              |
| 15h00-17h00 | [bloc 3](bloc3/Readme.md), TP | groupe 1 | A11           | Philippe, Thomas      |
|             |                               | groupe 2 | A12           | Laurent, Jean-Luc     |
|             |                               | groupe 3 | A13           | Benoit, Jean-Stéphane |


semaine du 17 juin 2019
=======================

du lundi au vendredi 

lundi 17 juin 2019
==================

matin : bloc 1
--------------

| quand       | quoi                         | qui              | où            | avec qui        |
|-------------|------------------------------|------------------|---------------|-----------------|
| 9h00-10h00  | Cours                        | tous les groupes | amphi Bacchus | Jean-Christophe |
| 10h15-12h15 | [bloc1](bloc1/Readme.md#17-juin), TP | groupe 1 |               | Benoit          |
|             |                              | groupe 2         |               | Jean-Christophe |
|             |                              | groupe 3         |               | Fabien          |
|             |                              |                  |               | Philippe        |



après-midi : bloc 2
-------------------
  
| quand       | quoi                     | qui           | où            | avec qui             |
|-------------|--------------------------|---------------|---------------|----------------------|
| 13h45-14h45 | Cours                    | tout le monde | amphi Bacchus | Éric                 |
| 15h00-17h00 | [bloc2](bloc2/Readme.md) | groupe 1      | A11           | Marie-Émilie, Benoit |
|             |                          | groupe 2      | A12           | Patricia, Bruno Bogaert |
|             |                          | groupe 3      | A13           | Éric, Jean-Stéphane  |
  
mardi 18 juin 2019
==================

matin : bloc 1
--------------

| quand       | quoi                         | qui              | où            | avec qui         |
|-------------|------------------------------|------------------|---------------|------------------|
| 9h00-10h00  | Cours                        | tous les groupes | amphi Bacchus | Jean-Christophe  |
| 10h15-12h15 | [bloc1](bloc1/Readme.md#18-juin), TD | groupe 1 | Axx           | Patricia, Mikaël |
|             |                              | groupe 2         | Axx           | JC, Fabien       |
|             |                              | groupe 3         | Axx           | Philippe, Maude  |
  		
		
après-midi : bloc 2
-------------------

| quand       | quoi  | qui           | où            | avec qui                |
|-------------|-------|---------------|---------------|-------------------------|
| 13h45-14h45 | Cours | tout le monde | amphi Bacchus | Sylvain                 |
| 15h00-17h00 | TD    | groupe 1      | A1            | Marie-Émilie            |
|             |       | groupe 2      | A2            | Patricia, Bruno Bogaert |
|             |       | groupe 3      | A3            | Éric, Laetitia          |
  

mercredi 19 juin 2019
=====================

matin : bloc 1
--------------

| quand      | quoi                         | qui      | où  | avec qui |
|------------|------------------------------|----------|-----|----------|
| 9h00-12h15 | [bloc1](bloc1/Readme.md), TP | groupe 1 | A11 | Patricia |
|            |                              | groupe 2 | A12 | Mikaël   |
|            |                              | groupe 3 | A13 | Maude    |
  
après-midi : bloc 3
-------------------

| quand       | quoi  | qui           | où    | avec qui         |
|-------------|-------|---------------|-------|------------------|
| 13h45-14h45 | Cours | tout le monde | amphi | Thomas           |
| 15h00-17h00 | TP    | groupe 1      | A11   | Yvan             |
|             |       | groupe 2      | A12   | Xavier, Jean-Luc |
|             |       | groupe 3      | A13   | Thomas           |

jeudi 20 juin 2019
==================

matin : bloc 1
--------------

| quand       | quoi                         | qui           | où            | avec qui |
|-------------|------------------------------|---------------|---------------|----------|
| 9h00-10h00  | Cours                        | tout le monde | amphi Bacchus | Mikaël   |
| 10h15-12h15 | [bloc1](bloc1/Readme.md), TP | groupe 1      | A11           |          |
|             |                              | groupe 2      | A12           |          |
|             |                              | groupe 3      | A13           |          |

après-midi : bloc 3
-------------------

| quand       | quoi       | qui           | où    | avec qui         |
|-------------|------------|---------------|-------|------------------|
| 13h45-14h45 | Conférence | tout le monde | amphi | Bruno / Philippe |
| 15h00-17h00 | TP         | groupe 1      | A11   | Pierre           |
|             |            | groupe 2      | A12   | Xavier           |
|             |            | groupe 3      | A13   | Thomas           |


vendredi 21 juin 2019
=====================

matin : bloc 2
--------------

| quand       | quoi  | qui              | où            | avec qui                |
|-------------|-------|------------------|---------------|-------------------------|
| 9h00-10h00  | Cours | tous les groupes | amphi Bacchus | Sylvain                 |
| 10h15-12h15 | TP    | groupe 1         | A11           | Marie-Émilie            |
|             |       | groupe 2         | A12           | Bruno Bogaert           |
|             |       | groupe 3         | A13           | Laetitia, Jean-Stéphane |
  
après-midi : conférences
------------------------

* (à venir)
* À partir de 16h30, présentation, exposition/démonstration par les
  jeunes filles de l'action _L codent L créent_. 

semaine du 1er juillet 2019
===========================

du lundi au jeudi 

lundi 1er juillet 2019
======================

* projet 

- ? Après-midi : bloc 2 ? 

mardi 2 juillet 2019
====================

mercredi 3 juillet 2019
=======================

- Après-midi : bloc 2

jeudi 4 juillet 2019
====================

* projet, suite et fin
