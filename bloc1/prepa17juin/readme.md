Travail préparatoire
====================

Afin de préparer au mieux le TP prévu la semaine du 17 juin et vous faire gagner un peu de temps en séance, on vous propose les activités de préparation suivantes. Elles  consistent essentiellement en de la prise d'information et il y a un exercice de programmation.

## namedtuple

Revoyez les tuples nommés avec `namedtuple`, présentés en cours le 5 juin.

 * [documentation officielle](https://docs.python.org/fr/3.7/library/collections.html#collections.namedtuple)
 * [autre ressource](https://www.reddit.com/r/Python/comments/38ee9d/intro_to_namedtuple/)
 * [encore une autre](https://www.tutorialspoint.com/namedtuple-in-python)

## dictionnaires

Les dictionnaires ont été rapidement présentés le 5 juin. Suite à plusieurs demandes, nous pourrons revenir rapidement sur cette notion le 17 juin. Mais vous pouvez déjà revoir cette notion, par exemple à travers le chapitre du livre de Swinnen ou [cette page du cours d'AP1](http://www.fil.univ-lille1.fr/~L1S2API/CoursTP/ensembles_et_dictionnaires.html).


## `str.split()` et autres méthodes du type `str`

Lisez la documentation sur la méthode `split()` de classe `str`.

D'une manière générale il est certainement intéressant pour vous de
découvrir [les méthodes de la classe `str`](https://docs.python.org/fr/3.7/library/stdtypes.html?highlight=split#string-methods) car la manipulation des
chaînes de caractères restent une activité courante en programmation.
Connaître les fonctionnalités offertes par le langage utilisé
évite donc souvent de redéfinir des méthodes existantes et permet d'accéder à des primitives utiles.

Vous pouvez aussi lire la première section du chapitre 10 du livre de Swinnen, en particulier la section 10.1.8.

Par exemple dans le TP les méthodes `rtstrip()`et `join()` pourront se révéler utile.

Lire la [documentation sur la méthode `format()`](https://docs.python.org/fr/3.7/library/string.html#format-string-syntax) est également pertinent. Si elle demande un petit investissement, celui-ci est vite rentable car il facilite la construction et le formatage des chaînes de caractères pour l'affichage.
La documentation se révèle, comme souvent,  un peu technique. Vous pouvez donc aussi consulter [ce site](https://pyformat.info/) ou encore [celui-ci](https://www.digitalocean.com/community/tutorials/how-to-use-string-formatters-in-python-3) ou aussi [celui-ci](https://www.programiz.com/python-programming/methods/string/format) et beaucoup d'autres encore...


## Manipulation de fichiers

Informez-vous sur la manipulation des fichiers en Python, en
particulier les fichiers texte. Vous pouvez par exemple étudier le
chapitre 9 du livre de Swinnen. Le
[Memento
Python3](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf)
 contient également une section sur les fichiers. Enfin, [cette page
 du cours
 d'AP1](http://www.fil.univ-lille1.fr/~L1S2API/CoursTP/fichiers.html)
 présente elle aussi les fichiers.
 

### À faire

Réalisez une fonction `recopie` à deux paramètres de type chaîne de
caractères. Le premier correspond au nom d'un fichier texte en entrée,
le second au nom d'un fichier en sortie. Cette fonction a pour effet
de lire chacune des lignes contenues dans le fichier texte d'entrée et de les recopier dans le second après avoir remplacé tous les `'o'` par des `'0'`.

*Suggestion* Jetez un œil à la méthode `replace` dans `str`.

## Exceptions

Les exceptions ne sont pas mentionnées dans le programme de NSI. Il est cependant difficile de programmer avec les langages "modernes" sans les rencontrer.

Il est donc certainement nécessaire que vous preniez connaissance de cette notion.

Vous pouvez la découvrir [dans la documentation python](https://docs.python.org/fr/3.6/tutorial/errors.html) ou [dans les notes du cours d'AP2](http://www.fil.univ-lille1.fr/~L2S3API/CoursTP/exceptions.html) ou [ici](https://realpython.com/python-exceptions/) ou encore [ici](http://sametmax.com/gestion-des-erreurs-en-python/).


## Lecture du sujet

Enfin, prendre connaissance du [sujet du TP](./../course_chicon/readme.md) en amont peut vous faire gagner du temps lors de la séance et vous permettre d'entrer plus vite dans le vif du sujet (et pas de panique à la lecture du sujet celui-ci devrait vous occuper à la fois une partie du lundi matin et du mercredi matin).
