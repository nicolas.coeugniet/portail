# Séance du17 juin

## les dictionnaires

Parcours rapide des fonctionnalités sur les dictionnaires.
On en profite pour évoquer quelques points complémentaires rapidement (`format`, `join`, exceptions, lambda).

 * [dictionnaire.py](./dictionnaire.py)

## retours sur du code

On revient sur quelques codes écrits à l'occasion du projet Wa-Tor et on en profite pour parler
de bonnes pratiques et discuter sur des critères d'éléments de qualité du code.
 
  * [exemples_codes_wator.md](./exemples_codes_wator.md)